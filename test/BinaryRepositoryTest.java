
import dao.Person;
import org.junit.jupiter.api.Test;
import repository.BinaryRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BinaryRepositoryTest {

    BinaryRepository binaryRepository = new BinaryRepository();

    @Test
    void createPerson_ShouldCreateNewPerson() {

        //given
        Person person = new Person("Dariia", "Kaptiurova", 20, "Kyiv");

        //when
        boolean actual = binaryRepository.createPerson(person);

        //then
        assertTrue(actual);
    }

    @Test
    void readAll_ShouldReadFile() {

        //given
        List<Person> expected = new ArrayList();

        //when
        String prepareTheFile = binaryRepository.deleteAll();
        List<Person> actual = binaryRepository.readAll();

        Person person = new Person("Dariia", "Kaptiurova", 20, "Kyiv");
        boolean prepareTheFileAgain = binaryRepository.createPerson(person);
        List<Person> actual1 = binaryRepository.readAll();

        //then
        assertEquals(expected, actual);
        assertNotEquals(expected, actual1);
    }

    @Test
    void updatePerson_ShouldUpdatePerson() {

        //given
        String expected = "Person was updated.";

        //when
        String actual = binaryRepository.updatePerson(1, "Dariia", "Kaptiurova", 20, "Kyiv");

        //then
        assertEquals(expected, actual);
    }

    @Test
    void deletePerson_ShouldDeletePerson() {

        //given
        String fieldName = "id";
        String fieldValue = "1";

        String fieldName1 = "firstName";
        String fieldValue1 = "Dariia";

        String fieldName2 = "lastName";
        String fieldValue2 = "Kaptiurova";

        String fieldName3 = "age";
        String fieldValue3 = "20";

        String fieldName4 = "city";
        String fieldValue4 = "Kyiv";

        String fieldName5 = "id";
        String fieldValue5 = "90";

        //when
        String expected = "Delete was successful!";
        String actual = binaryRepository.deletePerson(fieldName, fieldValue);

        String expected1 = "Delete was successful!";
        String actual1 = binaryRepository.deletePerson(fieldName1, fieldValue1);

        String expected2 = "Delete was successful!";
        String actual2 = binaryRepository.deletePerson(fieldName2, fieldValue2);

        String expected3 = "Delete was successful!";
        String actual3 = binaryRepository.deletePerson(fieldName3, fieldValue3);

        String expected4 = "Delete was successful!";
        String actual4 = binaryRepository.deletePerson(fieldName4, fieldValue4);

        String expected5 = "Enter value in ID-field!";
        String actual5 = binaryRepository.deletePerson(fieldName5, fieldValue5);

        //then
        assertEquals(expected, actual);
        assertEquals(expected1, actual1);
        assertEquals(expected2, actual2);
        assertEquals(expected3, actual3);
        assertEquals(expected4, actual4);
        assertNotEquals(expected5, actual5);
    }

    @Test
    void deleteAll_ShouldDeleteAllPeople() {

        //given

        //when
        String expected = "All persons were deleted.";
        String actual = binaryRepository.deleteAll();

        //then
        assertEquals(expected, actual);
    }

    @Test
    void createID_ShouldCreateID() {

        //given
        String prepareTheFile = binaryRepository.deleteAll();
        Person person = new Person("Dariia", "Kaptiurova", 20, "Kyiv");
        binaryRepository.createPerson(person);

        //when
        int expected = 2;
        int actual = binaryRepository.createID();

        int expected1 = 0;
        int actual1 = binaryRepository.createID();

        //then
        assertEquals(expected, actual);
        assertNotEquals(expected1, actual1);
    }
}