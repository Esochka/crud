
import dao.Person;
import org.junit.jupiter.api.Test;
import repository.MongoDBRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MongoDBRepositoryTest {

    MongoDBRepository mongoDBRepository = new MongoDBRepository();


    @Test
    public void createPersonTest() {

        //given
        Person person = new Person(1, "Dariia", "Kaptiurova", 20, "Kiev");

        //when
        boolean actual = mongoDBRepository.createPerson(person);

        //then
        assertTrue(actual);
    }

    @Test
    public void updatePersonTest() {

        //given
        String expected = "The changes were successful!";

        //when
        String actual = mongoDBRepository.updatePerson(1, "Dariia", "Kaptiurova", 20, "Kiev");

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void deleteALLPersonTest() {

        //given
        String expected = "Persons were deleted!";

        //when
        String actual = mongoDBRepository.deleteAll();

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void deleteALLPersonFieldTest() {

        //given
        String expected = "Delete was successful!";

        //when
        String actual = mongoDBRepository.deletePerson("id", "1");

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void readAllFailedTest() {

        List<Person> expected = new ArrayList<>();
        expected.add(new Person(11, "1", "2", 3, "4"));

        //when
        List<Person> actual = mongoDBRepository.readAll();

        //then
        assertNotEquals(expected.toString(), actual.toString());
    }
}