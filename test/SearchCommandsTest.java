import dao.Person;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import repository.SearchCommands;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class SearchCommandsTest {
    ArrayList<Person> test = new ArrayList<>();

    @BeforeEach
    public void createTestArray() {
        test.add(new Person(1, "Dariia", "Kaptiurova", 20, "Kyiv"));
        test.add(new Person(2, "Nikita", "Smirnov", 22, "Kyiv"));
    }

    @Test
    void findPersonByID_ShouldReturnPersonByIDPass() {

        //given
        String expected = "[Person{firstName='Dariia', lastName='Kaptiurova', age=20, city='Kyiv', id=1}]";
        int id = 1;
        String expected1 = "[]";
        int id1 = 10;

        //when
        List<Person> actual = SearchCommands.findPersonByID(id, test);
        List<Person> actual1 = SearchCommands.findPersonByID(id1, test);

        //then
        assertEquals(expected, actual.toString());
        assertEquals(expected1, actual1.toString());
    }


    @Test
    void findPersonById_ShouldReturnPersonByIDFailed() {

        //given
        String expected = "Dariia Kaptiurova, 20 years old, Kyiv city.";
        int id = 2;

        //when
        List<Person> actual = SearchCommands.findPersonByID(id, test);

        //then
        assertNotEquals(expected, actual.toString());
    }

    @Test
    void findPersonByFirstName_ShouldReturnPeopleByTheSameFirstNamePass() {

        //given
        String expected = "[Person{firstName='Dariia', lastName='Kaptiurova', age=20, city='Kyiv', id=1}]";
        String firstName = "Dariia";
        String expected1 = "[]";
        String firstName1 = "Andrew";

        //when
        List<Person> actual = SearchCommands.findPersonByFirstName(firstName, test);
        List<Person> actual1 = SearchCommands.findPersonByFirstName(firstName1, test);

        //then
        assertEquals(expected, actual.toString());
        assertEquals(expected1, actual1.toString());
    }


    @Test
    void findPersonByFirstName_ShouldReturnPeopleByTheSameFirstNameFailed() {

        //given
        String expected = "Dariia Kaptiurova, 20 years old.";
        String firstName = "Dariia";

        //when
        List<Person> actual =SearchCommands.findPersonByFirstName(firstName, test);

        //then
        assertNotEquals(expected, actual.toString());
    }

    @Test
    void findPersonByLastName_ShouldReturnPeopleByTheSameLastNamePass() {

        //given
        String expected = "[Person{firstName='Dariia', lastName='Kaptiurova', age=20, city='Kyiv', id=1}]";
        String lastName = "Kaptiurova";
        String expected1 = "[]";
        String lastName1 = "Taptunov";

        //when
        List<Person> actual = SearchCommands.findPersonByLastName(lastName, test);
        List<Person> actual1 = SearchCommands.findPersonByLastName(lastName1, test);

        //then
        assertEquals(expected, actual.toString());
        assertEquals(expected1, actual1.toString());
    }


    @Test
    void findPersonByLastName_ShouldReturnPeopleByTheSameLastNameFailed() {

        //given
        String expected = "Dariia Kaptiurova, 20 years old.";
        String lastName = "Kaptiurova";

        //when
        List<Person> actual = SearchCommands.findPersonByLastName(lastName, test);

        //then
        assertNotEquals(expected, actual.toString());
    }


    @Test
    void findPersonByAge_ShouldReturnPeopleByTheSameAgePass() {

        //given
        String expected = "[Person{firstName='Dariia', lastName='Kaptiurova', age=20, city='Kyiv', id=1}]";
        int age = 20;
        String expected1 = "[]";
        int age1 = 30;

        //when
        List<Person> actual = SearchCommands.findPersonByAge(age, test);
        List<Person> actual1 = SearchCommands.findPersonByAge(age1, test);

        //then
        assertEquals(expected, actual.toString());
        assertEquals(expected1, actual1.toString());
    }


    @Test
    void findPersonByAge_ShouldReturnPeopleByTheSameAgeFailed() {

        //given
        String expected = "Dariia Kaptiurova, Kyiv city." + "\n";
        int age = 25;

        //when
        List<Person> actual = SearchCommands.findPersonByAge(age, test);

        //then
        assertNotEquals(expected, actual.toString());
    }

    @Test
    void findPersonByCity_ShouldReturnPeopleByTheSameCityPass() {

        //given
        String expected = "[Person{firstName='Dariia', lastName='Kaptiurova', age=20, city='Kyiv', id=1}, Person{firstName='Nikita', lastName='Smirnov', age=22, city='Kyiv', id=2}]";
        String city = "Kyiv";
        String expected1 = "[]";
        String city1 = "Poltava";

        //when
        List<Person> actual = SearchCommands.findPersonByCity(city, test);
        List<Person> actual1 = SearchCommands.findPersonByCity(city1, test);

        //then
        assertEquals(expected, actual.toString());
        assertEquals(expected1, actual1.toString());
    }


    @Test
    void findPersonByCity_ShouldReturnPeopleByTheSameCityFailed() {

        //given
        String expected = "Nikita Smirnov, 22 years old.";
        String city = "Kyiv";

        //when
        List<Person> actual = SearchCommands.findPersonByCity(city, test);

        //then
        assertNotEquals(expected, actual);
    }
}
