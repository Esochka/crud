
import dao.Person;
import org.junit.jupiter.api.Test;
import repository.MySQLRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MySQLRepositoryTest {

    MySQLRepository mySQLRepository = new MySQLRepository();


    @Test
    public void createPersonTest() {

        //given
        Person person = new Person(1, "Dariia", "Kaptiurova", 20, "Kiev");

        //when
        boolean actual = mySQLRepository.createPerson(person);

        //then
        assertTrue(actual);
    }

    @Test
    public void updatePersonTest() {

        //given
        String expected = "The changes were successful!";

        //when
        String actual = mySQLRepository.updatePerson(1, "Dariia", "Kaptiurova", 20, "Kiev");

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void deleteALLPersonTest() {

        //given
        String expected = "Persons were deleted!";

        //when
        String actual = mySQLRepository.deleteAll();

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void deleteALLPersonFieldTest() {

        //given
        String expected = "Persons were deleted!";

        //when
        String actual = mySQLRepository.deletePerson("id", "1");

        //then
        assertEquals(expected, actual);
    }

    @Test
    public void readAllFailedTest() {

        List<Person> expected = new ArrayList<>();
        expected.add(new Person(11, "1", "2", 3, "4"));

        //when
        List<Person> actual = mySQLRepository.readAll();

        //then
        assertNotEquals(expected.toString(), actual.toString());
    }
}