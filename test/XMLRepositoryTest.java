
import dao.Person;
import org.junit.jupiter.api.Test;
import repository.XMLRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class XMLRepositoryTest {

    XMLRepository xmlRepository = new XMLRepository();

    @Test
    void createPerson_ShouldCreateNewPerson() {

        //given
        Person person = new Person("Dariia", "Kaptiurova", 20, "Kyiv");

        //when
        boolean actual = xmlRepository.createPerson(person);

        //then
        assertTrue(actual);
    }

    @Test
    void readAll_ShouldReadFile() {

        //given
        List<Person> expected = new ArrayList();

        //when
        String prepareTheFile = xmlRepository.deleteAll();
        List<Person> actual = xmlRepository.readAll();

        Person person = new Person("Dariia", "Kaptiurova", 20, "Kyiv");
        boolean prepareTheFileAgain = xmlRepository.createPerson(person);
        List<Person> actual1 = xmlRepository.readAll();

        //then
        assertEquals(expected, actual);
        assertNotEquals(expected, actual1);
    }

    @Test
    void updatePerson_ShouldUpdatePerson() {

        //given
        String expected = "The changes were successful!";

        //when
        String actual = xmlRepository.updatePerson(1, "Dariia", "Kaptiurova", 20, "Kyiv");

        //then
        assertEquals(expected, actual);
    }

    @Test
    void deletePerson_ShouldDeletePerson() {

        //given
        String fieldName = "id";
        String fieldValue = "1";

        String fieldName1 = "firstName";
        String fieldValue1 = "Dariia";

        String fieldName2 = "lastName";
        String fieldValue2 = "Kaptiurova";

        String fieldName3 = "age";
        String fieldValue3 = "20";

        String fieldName4 = "city";
        String fieldValue4 = "Kyiv";

        String fieldName5 = "id";
        String fieldValue5 = "90";

        //when
        String expected = "Delete was successful!";
        String actual = xmlRepository.deletePerson(fieldName, fieldValue);

        String expected1 = "Delete was successful!";
        String actual1 = xmlRepository.deletePerson(fieldName1, fieldValue1);

        String expected2 = "Delete was successful!";
        String actual2 = xmlRepository.deletePerson(fieldName2, fieldValue2);

        String expected3 = "Delete was successful!";
        String actual3 = xmlRepository.deletePerson(fieldName3, fieldValue3);

        String expected4 = "Delete was successful!";
        String actual4 = xmlRepository.deletePerson(fieldName4, fieldValue4);

        String expected5 = "Enter value in ID-field!";
        String actual5 = xmlRepository.deletePerson(fieldName5, fieldValue5);

        //then
        assertEquals(expected, actual);
        assertEquals(expected1, actual1);
        assertEquals(expected2, actual2);
        assertEquals(expected3, actual3);
        assertEquals(expected4, actual4);
        assertNotEquals(expected5, actual5);
    }

    @Test
    void deleteAll_ShouldDeleteAllPeople() {

        //given

        //when
        String expected = "All persons were deleted.";
        String actual = xmlRepository.deleteAll();

        //then
        assertEquals(expected, actual);
    }

    @Test
    void createID_ShouldCreateID() {

        //given
        String prepareTheFile = xmlRepository.deleteAll();
        Person person = new Person("Dariia", "Kaptiurova", 20, "Kyiv");
        xmlRepository.createPerson(person);

        //when
        int expected = 2;
        int actual = xmlRepository.createID();

        int expected1 = 0;
        int actual1 = xmlRepository.createID();

        //then
        assertEquals(expected, actual);
        assertNotEquals(expected1, actual1);
    }

    @Test
    void writeXMLConstructor() {
        //given
        Person person = new Person("Dariia", "Kaptiurova", 20, "Kyiv");
        String expected = "\n\t<Person>"
                + "\n\t <id>" + 2 + "</id>"
                + "\n\t <first_name>" + "Dariia" + "</first_name>"
                + "\n\t <second_name>" + "Kaptiurova" + "</second_name>"
                + "\n\t <age>" + 20 + "</age>"
                + "\n\t <city>" + "Kyiv" + "</city> " +
                "\n\t</Person>" + "\n\n  ";
        Person person1 = new Person("Dariia", "Kaptiurova", 20, "Kyiv");
        String expected1 = "Dariia, Kaptiurova";

        //when
        String actual = xmlRepository.writeXMLConstructor(person);
        String actual1 = xmlRepository.writeXMLConstructor(person1);

        //then
        assertEquals(expected, actual);
        assertNotEquals(expected1, actual1);
    }

    @Test
    void writeXMLConstructorFromUpdate() {

        //given
        Person person = new Person("Dariia", "Kaptiurova", 20, "Kyiv");
        String expected = "\n\t<Person>"
                + "\n\t <id>" + 0 + "</id>"
                + "\n\t <first_name>" + "Dariia" + "</first_name>"
                + "\n\t <second_name>" + "Kaptiurova" + "</second_name>"
                + "\n\t <age>" + 20 + "</age>"
                + "\n\t <city>" + "Kyiv" + "</city> " +
                "\n\t</Person>" + "\n\n  ";
        Person person1 = new Person("Dariia", "Kaptiurova", 20, "Kyiv");
        String expected1 = "Dariia, Kaptiurova";

        //when
        String actual = xmlRepository.writeXMLConstructorFromUpdate(person);
        String actual1 = xmlRepository.writeXMLConstructorFromUpdate(person1);

        //then
        assertEquals(expected, actual);
        assertNotEquals(expected1, actual1);
    }
}