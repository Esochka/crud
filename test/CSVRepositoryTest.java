
import dao.Person;
import org.junit.jupiter.api.Test;
import repository.CSVRepository;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CSVRepositoryTest {

    CSVRepository csvUtil = new CSVRepository();

    @Test
    void createPerson_ShouldCreateNewPerson() {

        //given
        Person person = new Person("Dariia", "Kaptiurova", 20, "Kyiv");

        //when
        boolean actual = csvUtil.createPerson(person);

        //then
        assertTrue(actual);
    }

    @Test
    void readAll_ShouldReadFile() {

        //given
        List<Person> expected = new ArrayList();

        //when
        String prepareTheFile = csvUtil.deleteAll();
        List<Person> actual = csvUtil.readAll();

        Person person = new Person("Dariia", "Kaptiurova", 20, "Kyiv");
        boolean prepareTheFileAgain = csvUtil.createPerson(person);
        List<Person> actual1 = csvUtil.readAll();

        //then
        assertEquals(expected, actual);
        assertNotEquals(expected, actual1);
    }

    @Test
    void updatePerson_ShouldUpdatePerson() {

        //given
        String expected = "The changes were successful!";

        //when
        String actual = csvUtil.updatePerson(1, "Dariia", "Kaptiurova", 20, "Kyiv");

        //then
        assertEquals(expected, actual);
    }

    @Test
    void deletePerson_ShouldDeletePerson() {

        //given
        String fieldName = "id";
        String fieldValue = "1";

        String fieldName1 = "firstName";
        String fieldValue1 = "Dariia";

        String fieldName2 = "lastName";
        String fieldValue2 = "Kaptiurova";

        String fieldName3 = "age";
        String fieldValue3 = "20";

        String fieldName4 = "city";
        String fieldValue4 = "Kyiv";

        String fieldName5 = "id";
        String fieldValue5 = "90";

        //when
        String expected = "Delete was successful!";
        String actual = csvUtil.deletePerson(fieldName, fieldValue);

        String expected1 = "Delete was successful!";
        String actual1 = csvUtil.deletePerson(fieldName1, fieldValue1);

        String expected2 = "Delete was successful!";
        String actual2 = csvUtil.deletePerson(fieldName2, fieldValue2);

        String expected3 = "Delete was successful!";
        String actual3 = csvUtil.deletePerson(fieldName3, fieldValue3);

        String expected4 = "Delete was successful!";
        String actual4 = csvUtil.deletePerson(fieldName4, fieldValue4);

        String expected5 = "Enter value in ID-field!";
        String actual5 = csvUtil.deletePerson(fieldName5, fieldValue5);

        //then
        assertEquals(expected, actual);
        assertEquals(expected1, actual1);
        assertEquals(expected2, actual2);
        assertEquals(expected3, actual3);
        assertEquals(expected4, actual4);
        assertNotEquals(expected5, actual5);
    }

    @Test
    void deleteAll_ShouldDeleteAllPeople() {

        //given

        //when
        String expected = "All persons were deleted.";
        String actual = csvUtil.deleteAll();

        //then
        assertEquals(expected, actual);
    }

    @Test
    void createID_ShouldCreateID() {

        //given
        String prepareTheFile = csvUtil.deleteAll();
        Person person = new Person("Dariia", "Kaptiurova", 20, "Kyiv");
        csvUtil.createPerson(person);

        //when
        int expected = 2;
        int actual = csvUtil.createID();

        int expected1 = 0;
        int actual1 = csvUtil.createID();

        //then
        assertEquals(expected, actual);
        assertNotEquals(expected1, actual1);
    }

    @Test
    void toStringCSV() {

        //given
        Person person = new Person("Dariia", "Kaptiurova", 20, "Kyiv");
        String expected =  " id=" + 1 +
                ", firstName=" + "Dariia" +
                ", lastName=" + "Kaptiurova" +
                ", age=" + 20 +
                ", city=" + "Kyiv" + "\n";
        Person person1 = new Person("Dariia", "Kaptiurova", 20, "Kyiv");
        String expected1 = "Dariia, Kaptiurova";

        //when
        String actual = csvUtil.toStringCSV(person);
        String actual1 = csvUtil.toStringCSV(person);

        //then
        assertEquals(expected, actual);
        assertNotEquals(expected1, actual1);
    }

    @Test
    void toStringCSVForUpdate() {

        //given
        Person person = new Person("Dariia", "Kaptiurova", 20, "Kyiv");
        String expected =  " id=" + 0 +
                ", firstName=" + "Dariia" +
                ", lastName=" + "Kaptiurova" +
                ", age=" + 20 +
                ", city=" + "Kyiv" + "\n";
        Person person1 = new Person("Dariia", "Kaptiurova", 20, "Kyiv");
        String expected1 = "Dariia, Kaptiurova";

        //when
        String actual = csvUtil.toStringCSVForUpdate(person);
        String actual1 = csvUtil.toStringCSVForUpdate(person);

        //then
        assertEquals(expected, actual);
        assertNotEquals(expected1, actual1);
    }
}