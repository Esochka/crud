package utils;

public class Constants {

    public static final String CREATE_TABLE_PERSON = "CREATE TABLE person(" +
            "id BIGINT AUTO_INCREMENT PRIMARY KEY," +
            "firstName VARCHAR(255) NOT NULL," +
            "lastName VARCHAR(255) NOT NULL," +
            "age INTEGER NOT NULL DEFAULT 25," +
            "city VARCHAR(255) NOT NULL)";
    public static final String GET_ALL_PERSONS = "SELECT * FROM person";
    public static final String INSERT = "INSERT INTO person VALUES";
    public static final String ERROR = "Something went wrong...";
    public static final String SUCCESS = "Success";
    public static final String DELETE_ALL_PERSONS = "DELETE FROM person";
    public static final String DELETE_ALL_BY_FIRST_NAME = "DELETE FROM person WHERE firstName =";
    public static final String DELETE_ALL_BY_LAST_NAME = "DELETE FROM person WHERE lastName =";
    public static final String DELETE_ALL_BY_AGE = "DELETE FROM person WHERE age =";
    public static final String DELETE_ALL_BY_CITY = "DELETE FROM person WHERE city =";
    public static final String UPDATE_TABLE_PERSON = "UPDATE person SET ";
    public static final String WHERE_STATEMENT = "WHERE id =";
    public static final String DELETE = "DELETE FROM person WHERE id =";
    public static final String SUCCESS_DELETE = "Deleting was successful";
    public static final String SUCCESS_UPDATE = "Updating was successful";

}
