package dao;

public class Person {
    private int id;
    private String firstName;
    private String lastName;
    private int age;
    private String city;

    public Person() {
    }

    public Person(String firstNameUser, String lastNameUser, int ageUser, String cityUser) {
        firstName = firstNameUser;
        lastName = lastNameUser;
        age = ageUser;
        city = cityUser;
    }

    public Person(int idUser, String firstNameUser, String lastNameUser, int ageUser, String cityUser) {
        id = idUser;
        firstName = firstNameUser;
        lastName = lastNameUser;
        age = ageUser;
        city = cityUser;
    }

    // Блок геттеров
    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getAge() {
        return age;
    }

    public String getCity() {
        return city;
    }

    // Блок сеттеров
    public void setId(int id) {
        this.id = id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Person{");
        sb.append("firstName='").append(firstName).append('\'');
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append(", age=").append(age);
        sb.append(", city='").append(city).append('\'');
        sb.append(", id=").append(id);
        sb.append('}');
        return sb.toString();
    }

    public String toQueryString(){
        return "(" +
                this.id + "," +
                "'" + this.firstName + "'" + "," +
                "'" + this.lastName + "'" + "," +
                this.age + "," +
                "'" +this.city + "'" +
                ")";
    }

    public String toDeleteQueryString(){
        return "firstName=" + "'" + this.firstName + "'" + "," +
                "lastName=" + "'" + this.lastName + "'" + "," +
                "age=" + this.age + "," +
                "city=" + "'" + this.city + "'";
    }
}
