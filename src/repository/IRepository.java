package repository;


import dao.Person;

import java.util.List;

public interface IRepository {

    boolean createPerson(Person person);
    List<Person> readAll();
    String updatePerson(int id, String fistName, String lastName, int age, String city);
    String deletePerson(String fieldName, String fieldValue);
    String deleteAll();
}
