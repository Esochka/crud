package repository;

import dao.Person;
import org.h2.tools.DeleteDbFiles;
import utils.Constants;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class H2Context {
    public static final String DB_DIR = "D://Documents//DevEducation//DreamTeamProj//db";
    public static final String DB_FILE = "stockExchange";
    public static final String DB_URL = "jdbc:h2:/" + DB_DIR + "/" + DB_FILE;
    public static final String DB_Driver = "org.h2.Driver";
    Person person;

    public H2Context(boolean renew)throws SQLException, ClassNotFoundException{
        if (renew)
            DeleteDbFiles.execute(DB_DIR, DB_FILE, false);
        Class.forName(DB_Driver);
        // Инициализируем таблицы
        person = new Person();
    }

    public static Connection Connect(){
        try {
            Class.forName(DB_Driver); //Проверяем наличие JDBC драйвера для работы с БД
            var connection= DriverManager.getConnection(DB_URL);//соединениесБД
            System.out.println("Соединение с СУБД выполнено.");

            return connection;

        } catch (ClassNotFoundException e) {
            e.printStackTrace(); // обработка ошибки  Class.forName
            System.out.println("JDBC драйвер для СУБД не найден!");
            return null;
        } catch (SQLException e) {
            e.printStackTrace(); // обработка ошибок  DriverManager.getConnection
            System.out.println(Constants.ERROR);
            return null;
        }
    }

}

