package repository;


import dao.Person;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class XMLRepository implements IRepository {

    public static File file = new File("persons.xml");

    public boolean createPerson(Person person) {
        try {

            FileWriter fileWriter = new FileWriter(file, true);
            FileReader reader = new FileReader(file);

            if (reader.read() == -1) {

                String result = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                        + "\n<controller.Main>" + writeXMLConstructor(person) + "</controller.Main>";

                fileWriter.write(result);
                fileWriter.close();
            } else {
                List<Person> personToAdd = readAll();
                StringBuilder result = new StringBuilder();

                for (Person splitterPerson : personToAdd) {
                    result.append(writeXMLConstructorFromUpdate(splitterPerson));
                }
                result.append(writeXMLConstructor(person)).append("</controller.Main>");
                result = new StringBuilder(result.toString().replaceAll(" , 	</city>", ""));
                PrintWriter writer = new PrintWriter(file);
                writer.print("");
                fileWriter.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                        + "\n<controller.Main>" + result);
                fileWriter.close();
            }
        } catch (
                IOException e) {
            return false;
        }
        return true;
    }

    public List<Person> readAll() {
        ArrayList<Person> listOfPersons = new ArrayList<>();
        try {
            FileWriter fileWriter = new FileWriter(file, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            String line = (Files.readAllLines(file.toPath())).toString();
            String[] arrayLine = line.split("</Person>");
            for (String splitterPerson : arrayLine) {
                if (splitterPerson.equals(", ,   </controller.Main>]")) {
                    break;
                }
                if (splitterPerson.equals("[]")) {
                    break;
                }
//                System.out.println(s);
                int id = Integer.parseInt(splitterPerson.substring(splitterPerson.indexOf("id") + 3,
                        splitterPerson.indexOf("</id>")));

                String firstName = splitterPerson.substring(splitterPerson.indexOf("first_name") + 11,
                        splitterPerson.indexOf("</first_name>"));

                String lastName = splitterPerson.substring(splitterPerson.indexOf("second_name") + 12,
                        splitterPerson.indexOf("</second_name>"));

                int age = Integer.parseInt(splitterPerson.substring(splitterPerson.indexOf("age") + 4,
                        splitterPerson.indexOf("</age>")));

                String city = splitterPerson.substring(splitterPerson.indexOf("city") + 5,
                        splitterPerson.indexOf("</city>"));

                Person person = new Person(id, firstName, lastName, age, city);
                listOfPersons.add(person);

            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return listOfPersons;
    }

    public String updatePerson(int id, String firstName, String lastName, int age, String city) {
        List<Person> personToUpdate = readAll();

        for (Person splitterPersonBeforeUpdate : personToUpdate) {
            if (splitterPersonBeforeUpdate.getId() == id) {
                splitterPersonBeforeUpdate.setFirstName(firstName);
                splitterPersonBeforeUpdate.setLastName(lastName);
                splitterPersonBeforeUpdate.setAge(age);
                splitterPersonBeforeUpdate.setCity(city);
            }
        }
        try {
            FileWriter writer = new FileWriter(file);

            StringBuilder result = new StringBuilder();

            for (Person splitterPersonAfterUpdate : personToUpdate) {
                result.append(writeXMLConstructorFromUpdate(splitterPersonAfterUpdate)).append(",");
            }
            result = new StringBuilder(result.toString().replaceAll(",", ""));
            writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                    + "\n<controller.Main>" + result + "</controller.Main>");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Person is updated.";
    }

    public String deletePerson(String fieldName, String fieldValue) {
        final String WRONG_MESSAGE = "Wrong entered id!";
        final String SUCCESS_MESSAGE = "Delete was success!";
        List<Person> personToDelete = readAll();

        switch (fieldName) {
            case "id":
                try {
                    int id = Integer.parseInt(fieldValue);
                    personToDelete.removeIf(person -> person.getId() == id);
                    FileWriter writer = new FileWriter(file);

                    StringBuilder result = new StringBuilder();

                    for (Person splitterPersonAfterDelete : personToDelete) {
                        result.append(writeXMLConstructorFromUpdate(splitterPersonAfterDelete));
                    }
                    if (result.toString().equals("")) {
                        writer.write(result.toString());
                    } else {
                        writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                                + "\n<controller.Main>" + result + "</controller.Main>");
                    }
                    writer.close();
                    return SUCCESS_MESSAGE;
                } catch (NumberFormatException e) {
                    return WRONG_MESSAGE;
                } catch (IOException e) {
                    return "File not found!";
                }

            case "firstName":
                try {
                    personToDelete.removeIf(person -> person.getFirstName().equals(fieldValue));
                    FileWriter writer = new FileWriter(file);

                    StringBuilder result = new StringBuilder();

                    for (Person splitterPersonAfterDelete : personToDelete) {
                        result.append(writeXMLConstructorFromUpdate(splitterPersonAfterDelete));
                    }
                    if (result.toString().equals("")) {
                        writer.write(result.toString());
                    } else {
                        writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                                + "\n<controller.Main>" + result + "</controller.Main>");
                    }
                    writer.close();
                    return SUCCESS_MESSAGE;
                } catch (IOException e) {
                    return "File not found!";
                }

            case "lastName":
                try {
                    personToDelete.removeIf(person -> person.getLastName().equals(fieldValue));
                    FileWriter writer = new FileWriter(file);

                    StringBuilder result = new StringBuilder();

                    for (Person splitterPersonAfterDelete : personToDelete) {
                        result.append(writeXMLConstructorFromUpdate(splitterPersonAfterDelete));
                    }
                    if (result.toString().equals("")) {
                        writer.write(result.toString());
                    } else {
                        writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                                + "\n<controller.Main>" + result + "</controller.Main>");
                    }
                    writer.close();
                    return SUCCESS_MESSAGE;
                } catch (IOException e) {
                    return "File not found!";
                }
            case "age":
                try {
                    int age = Integer.parseInt(fieldValue);
                    personToDelete.removeIf(person -> person.getAge() == age);
                    FileWriter writer = new FileWriter(file);

                    StringBuilder result = new StringBuilder();

                    for (Person splitterPersonAfterDelete : personToDelete) {
                        result.append(writeXMLConstructorFromUpdate(splitterPersonAfterDelete));
                    }
                    if (result.toString().equals("")) {
                        writer.write(result.toString());
                    } else {
                        writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                                + "\n<controller.Main>" + result + "</controller.Main>");
                    }
                    writer.close();
                    return SUCCESS_MESSAGE;
                } catch (NumberFormatException e) {
                    return WRONG_MESSAGE;
                } catch (IOException e) {
                    return "File not found!";
                }

            case "city":
                try {
                    personToDelete.removeIf(person -> person.getCity().equals(fieldValue));
                    FileWriter writer = new FileWriter(file);

                    StringBuilder result = new StringBuilder();

                    for (Person splitterPersonAfterDelete : personToDelete) {
                        result.append(writeXMLConstructorFromUpdate(splitterPersonAfterDelete));
                    }
                    if (result.toString().equals("")) {
                        writer.write(result.toString());
                    } else {
                        writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                                + "\n<controller.Main>" + result + "</controller.Main>");
                    }
                    writer.close();
                    return SUCCESS_MESSAGE;
                } catch (IOException e) {
                    return "File not found!";
                }
            default:
                return "Enter value in ID-field!";
        }
    }

    public String deleteAll() {
        try {
            PrintWriter writer = new PrintWriter(file);
            writer.print("");
            writer.close();
        } catch (IOException e) {
            return "Error.";
        }
        return "All persons deleted.";
    }


//    public String deletePerson(String fieldName, String fieldValue) {
//        List<Person> personToDelete = readAll();
//        personToDelete.removeIf(person -> person.getId() == id);
//
//        try {
//            FileWriter writer = new FileWriter(file);
//
//            String result = "";
//
//            for (Person splitterPersonAfterDelete : personToDelete) {
//                result += writeXMLConstructor(splitterPersonAfterDelete);
//            }
//            if (result == "") {
//                writer.write(result);
//            } else {
//                writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>" + "\n<controller.Main>" + result + "</controller.Main>");
//            }
//            writer.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    //    public static String displayAll() {
//        ArrayList<Person> allPersons = readFromXML();
//        StringBuilder displayPerson = new StringBuilder();
//        for (Person splitterPerson : allPersons) {
//            displayPerson.append(splitterPerson.toString());
//        }
//        return displayPerson.toString();
//
//    }
//
    public int createID() {
        List<Person> persons = readAll();
        try {
            return persons.get(persons.size() - 1).getId() + 1;
        } catch (Exception e) {
            return persons.size() + 1;
        }
    }

//    public static boolean checkId(int id) {
//        ArrayList<Person> personsList = readFromXML();
//        for (Person splitterPerson : personsList) {
//            if (splitterPerson.getId() == id) {
//                return true;
//            }
//        }
//        return false;
//    }

    public String writeXMLConstructor(Person person) {

        return "\n\t<Person>"
                + "\n\t <id>" + createID() + "</id>"
                + "\n\t <first_name>" + person.getFirstName() + "</first_name>"
                + "\n\t <second_name>" + person.getLastName() + "</second_name>"
                + "\n\t <age>" + person.getAge() + "</age>"
                + "\n\t <city>" + person.getCity() + "</city> " +
                "\n\t</Person>" + "\n\n  ";
    }

    public String writeXMLConstructorFromUpdate(Person person) {

        return "\n\t<Person>"
                + "\n\t <id>" + person.getId() + "</id>"
                + "\n\t <first_name>" + person.getFirstName() + "</first_name>"
                + "\n\t <second_name>" + person.getLastName() + "</second_name>"
                + "\n\t <age>" + person.getAge() + "</age>"
                + "\n\t <city>" + person.getCity() + "</city> " +
                "\n\t</Person>" + "\n\n  ";
    }

//    public  ObservableList<Person> converterStringToObservableList() {
//        return FXCollections.observableArrayList(readAll());
//    }

}

