package repository;


import dao.Person;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;


public class RedisRepository implements IRepository {
    private static Jedis jedis;
    final String URL = "c6.eu-west-1-1.ec2.cloud.redislabs.com";
    final int PORT = 19314;
    final String PASSWORD = "9MBiUUsEsLPFXS356RHEMIibhHemKlJ6";

    public RedisRepository(){
        jedis = new Jedis(URL, PORT);
        jedis.auth(PASSWORD);
    }

    public boolean createPerson(Person person) {
        jedis.sadd("persons", personToRedisFormat(person));
        return true;
    }


    public List<Person> readAll() {
        ArrayList<Person> listOfPersons = new ArrayList<>();
        for (String member : jedis.smembers("persons")) {
            String[] info = member.split(",");
            listOfPersons.add(new Person(Integer.parseInt(info[0]), info[1], info[2], Integer.parseInt(info[3]), info[4]));//try/catch
        }
        return listOfPersons;
    }

    @Override
    public String updatePerson(int id, String firstName, String lastName, int age, String city) {
        Person person = findPersonByID(id);
        if (person == null) {
            return "Person with this ID does not exist!";
        } else {
            String temp = personToRedisFormat(person);
            jedis.srem("persons", temp);
            StringBuilder sb = new StringBuilder(person.getId());
            if (firstName != null) {
                person.setFirstName(firstName);
                sb.append(person.getFirstName());
            }
            if (lastName != null) {
                person.setLastName(lastName);
                sb.append(person.getLastName());
            }
            if (age != 0) {
                person.setAge(age);
                sb.append(person.getAge());
            }
            if (city != null) {
                person.setCity(city);
                sb.append(person.getCity());
            }
            jedis.sadd("persons", personToRedisFormat(person));
            return "Successfully update!";
        }
    }


    public String deletePerson(String fieldName, String fieldValue) {
        final String WRONG_MESSAGE = "Wrong entered id!";
        final String ID_DOES_NOT_EXIST = "Entered ID does not exist!";
        final String SUCCESS_MESSAGE = "Delete was success!";
        switch (fieldName) {
            case "id":
                int id;
                try {
                    id = Integer.parseInt(fieldValue);
                } catch (NumberFormatException e) {
                    return WRONG_MESSAGE;
                }
                Person personByID = findPersonByID(id);
                if (personByID != null) {
                    jedis.srem("persons", personToRedisFormatForDelete(personByID));
                    return "Delete was success";
                } else {
                    return ID_DOES_NOT_EXIST;
                }

            case "firstName":
                List<Person> personByFirstName = findPersonByFirstName(fieldValue);
                if (personByFirstName != null) {
                    for (Person person : personByFirstName) {
                        jedis.srem("persons", personToRedisFormatForDelete(person));
                    }
                    return SUCCESS_MESSAGE;
                } else {
                    return ID_DOES_NOT_EXIST;
                }

            case "lastName":
                List<Person> personByLastName = findPersonByLastName(fieldValue);
                if (personByLastName != null) {
                    for (Person person : personByLastName) {
                        jedis.srem("persons", personToRedisFormatForDelete(person));
                    }
                    return SUCCESS_MESSAGE;
                } else {
                    return ID_DOES_NOT_EXIST;
                }

            case "age":
                int age;
                try {
                    age = Integer.parseInt(fieldValue);
                } catch (NumberFormatException e) {
                    return WRONG_MESSAGE;
                }
                List<Person> personsByAge = findPersonByAge(age);
                if (personsByAge != null) {
                    for (Person person : personsByAge) {
                        jedis.srem("persons", personToRedisFormatForDelete(person));
                    }
                    return SUCCESS_MESSAGE;
                } else {
                    return ID_DOES_NOT_EXIST;
                }

            case "city":
                List<Person> personByCity = findPersonByCity(fieldValue);
                if (personByCity != null) {
                    for (Person person : personByCity) {
                        jedis.srem("persons", personToRedisFormatForDelete(person));
                    }
                    return SUCCESS_MESSAGE;
                } else {
                    return ID_DOES_NOT_EXIST;
                }
            default:
                return "Enter value in ID-field!";
        }
    }

    @Override
    public String deleteAll() {
        jedis.flushAll();
        return "All persons were deleted";
    }


    private Person findPersonByID(int id) {
        Person person = readAll().stream()
                .filter(person1 -> person1.getId() == id)
                .findAny()
                .orElse(null);
        return person;
    }

    private List<Person> findPersonByFirstName(String firstName) {
        List<Person> persons = readAll().stream()
                .filter(person -> person.getFirstName().equals(firstName))
                .collect(Collectors.toList());
        return persons;
    }

    private List<Person> findPersonByLastName(String lastName) {
        List<Person> persons = readAll().stream()
                .filter(person -> person.getFirstName().equals(lastName))
                .collect(Collectors.toList());
        return persons;
    }

    private List<Person> findPersonByAge(int age) {
        List<Person> persons = readAll().stream()
                .filter(person1 -> person1.getAge() == age)
                .collect(Collectors.toList());
        return persons;
    }

    private List<Person> findPersonByCity(String city) {
        List<Person> persons = readAll().stream()
                .filter(person -> person.getCity().equals(city))
                .collect(Collectors.toList());
        return persons;
    }

    private String personToRedisFormat(Person person) {
        return createID() + "," + person.getFirstName() + "," + person.getLastName() + "," + person.getAge() + "," + person.getCity();
    }
    private String personToRedisFormatForDelete(Person person) {
        return person.getId() + "," + person.getFirstName() + "," + person.getLastName() + "," + person.getAge() + "," + person.getCity();
    }

    public int createID() {
        List<Person>  persons = readAll();
        persons.sort(comparing(Person::getId));

        try {
            return persons.get(persons.size() - 1).getId() + 1;
        }catch (Exception e){
            return persons.size() + 1;
        }
    }


}
